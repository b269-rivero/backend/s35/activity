const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;


mongoose.connect("mongodb+srv://patrickkevinrivero:admin123@zuitt-bootcamp.sfjvxno.mongodb.net/s35activity?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
// Model
const User = mongoose.model("User", userSchema);


	app.post("/signup", (req, res) => {
		User.findOne({username: req.body.username}).then((result, err) => {
			let newUser = new User({
			username: req.body.username,
			password: req.body.password
				});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered!");
				}
			})
		})
	});


app.listen(port, () => console.log(`Server running at port ${port}`));

